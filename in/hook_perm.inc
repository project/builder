<?php
$out .= <<<EOS

/**
 * hook_perm()
 * cf. http://api.drupal.org/api/function/node_example_perm
 */
function {$module_name}_perm() {
  return array('create {$module_name} node', 'edit own {$module_name} nodes');
}

EOS;
