<?php
$has_title_text = $has_title ? 'TRUE' : 'FALSE';
$has_body_text = $has_body ? 'TRUE' : 'FALSE';

$out .= <<<EOS

/**
 * hook_node_info()
 * cf. http://api.drupal.org/api/function/node_example_node_info
 */
function {$module_name}_node_info() {
  \$info = array();
  \$info['{$type_name}'] = array(
    'name' => '{$type_name}',           // required
    'module' => '{$module_name}',       // required
    'description' => '{$description}',  // required
    'help' => '{$help}',
    'has_title' => {$has_title_text},
    'title_label' => '{$title_label}',
    'has_body' => {$has_body_text},
    'body_label' => '{$body_label}',
  );
  return \$info;
}

EOS;
