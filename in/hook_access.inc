<?php
$out .= <<<EOS

/**
 * hook_access()
 * cf. http://api.drupal.org/api/function/node_example_access
 */
function {$module_name}_access(\$op, \$node) {
  global \$user;

  if (\$op == 'create') {
    // Only users with permission to do so may create this node type.
    return user_access('create {$type_name} nodes');
  }

  // Users who create a node may edit or delete it later, assuming they have the
  // necessary permissions.
  if (\$op == 'update' || \$op == 'delete') {
    if (user_access('edit own {$type_name} nodes') && (\$user->uid == \$node->uid)) {
      return TRUE;
    }
  }
}

EOS;
