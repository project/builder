<?php
$out .= <<<EOS

/**
 * hook_view()
 * cf. http://api.drupal.org/api/function/node_example_view
 */
function {$module_name}_view(\$node, \$teaser = FALSE, \$page = FALSE) {
  \$node = node_prepare(\$node, \$teaser);
  \$node->content['myfield'] = array(
    '#value' => theme('node_example_order_info', \$node),
    '#weight' => 1,
  );

  return \$node;
}

EOS;
