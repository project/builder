<?php
$out .= <<<EOS

/**
 * hook_insert()
 * cf. http://api.drupal.org/api/function/node_example_insert
 */
function {$module_name}_insert(\$node) {
  drupal_write_record('{$module_name}', \$node);
}

EOS;
