<?php
$out .= <<<EOS

/**
 * hook_theme()
 * cf. http://api.drupal.org/api/function/node_example_theme
 */
function {$module_name}_theme() {
  return array(
    '{$module_name}_order_info' => array(
      'arguments' => array('node'),
    ),
  );
}

EOS;
