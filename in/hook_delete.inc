<?php
$out .= <<<EOS

/**
 * hook_delete()
 * cf. http://api.drupal.org/api/function/node_example_delete
 */
function {$module_name}_delete(\$node) {
  // Notice that we're matching all revision, by using the node's nid.
  db_query('DELETE FROM {{$module_name}} WHERE nid = %d', \$node->nid);
}

EOS;
