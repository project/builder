<?php
$out .= <<<EOS

/**
 * hook_load()
 * cf. http://api.drupal.org/api/function/node_example_load
 */
function {$module_name}_load(\$node) {
  \$fields = drupal_schema_fields_sql('{$module_name}');
  \$additions = db_fetch_object(db_query(
    'SELECT ' . join(',', \$fields) . ' FROM {{$module_name}} WHERE vid = %d', 
    \$node->vid));

  return \$additions;
}

EOS;
