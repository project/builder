<?php
$out .= <<<EOS

/**
 * hook_nodeapi()
 *
 * When a node revision is deleted, we need to remove the corresponding record from
 * our table. The only way to handle revision deletion is by implementing 
 * hook_nodeapi().
 *
 * cf. http://api.drupal.org/api/function/node_example_nodeapi
 */
function {$module_name}_nodeapi(&\$node, \$op, \$teaser, \$page) {
  switch (\$op) {
    case 'delete revision':
      // Notice that we're matching a single revision based on the node's vid.
      db_query('DELETE FROM {{$module_name}} WHERE vid = %d', \$node->vid);
      break;
  }
}

EOS;
