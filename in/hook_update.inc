<?php
$out .= <<<EOS

/**
 * hook_update()
 * cf. http://api.drupal.org/api/function/node_example_update
 */
function {$module_name}_update(\$node) {
  // if this is a new node or we're adding a new revision,
  if (\$node->revision) {
    {$module_name}_insert(\$node);
  }
  else {
    drupal_write_record('{$module_name}', \$node, array('vid'));
  }
}

EOS;
