<?php
$out .= <<<EOS

/**
 * hook_update()
 * cf. http://api.drupal.org/api/function/node_example_update
 */
function {$module_name}_validate(\$node) {
  if (\$node->quantity) {
    if (!is_numeric(\$node->quantity)) {
      form_set_error('quantity', t('The quantity must be a number.'));
    }
  }
  else {
    // Let an empty field mean "zero."
    \$node->quantity = 0;
  }
}

EOS;
