<?php
$out .= <<<EOS

/**
 * hook_form()
 * cf. http://api.drupal.org/api/function/node_example_form
 */
function {$module_name}_form(&\$node) {


EOS;

if ($has_title) {
  // we already KNOW that the type has a title, so why bother checking
  
  $out .= <<<EOS
  \$form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('{$title_label}'),
    '#required' => TRUE,
    '#default_value' => \$node->title,
    '#weight' => -5
  );

EOS;
}

if ($has_body) {
  // we already KNOW that the type has a body, so why bother checking
  
  $body_required = $min_word_count > 0 ? 'TRUE' : 'FALSE';
  
  $out .= <<<EOS
  // We want the body and filter elements to be adjacent. We could try doing
  // this by setting their weights, but another module might add elements to
  // the form with the same weights and end up between ours. By putting them
  // into a sub-array together, we're able force them to be rendered together.
  \$form['body_filter']['body'] = array(
    '#type' => 'textarea',
    '#title' => t('{$body_label}'),
    '#default_value' => \$node->body,
    '#required' => {$body_required},
  );

  \$form['body_filter']['format'] = filter_form(\$node->format);


EOS;
}

$out .= <<<EOS
  // Now we define the form elements specific to our node type.
{$form_fields}
  return \$form;
}

EOS;
