<?php
// $Id

function builder_admin_settings(&$form_state) {
  $form['builder_config_dir'] = array(
    '#type' => 'textfield',
    '#title' => t('Config directory'),
    '#size' => 30,
    '#maxlength' => 64,
    '#description' => t('Directory where to find the configuration files.'),
    '#default_value'=> variable_get('builder_config_dir', 'config'),
  );

  $form['builder_module_dir'] = array(
    '#type' => 'textfield',
    '#title' => t('Module directory'),
    '#size' => 30,
    '#maxlength' => 64,
    '#description' => t('Directory where to put generated modules.'),
    '#default_value'=> variable_get('builder_module_dir', 'modules'),
  );

  builder_get_config_files();
  return system_settings_form($form);
}

